<% import grails.persistence.Event %>
<%=packageName%>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
    <title><g:message code="default.show.label" args="[entityName]" /></title>
</head>
<body>
<g:if test="\${flash.message}">
    <div class="alert alert-info alert-dismissible fade in col-md-offset-8" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong>\${flash.message}</strong>
    </div>
</g:if>
<div id="show-${domainClass.propertyName}" class="content scaffold-show" role="main">
    <div class="col-md-2">
        <div class="btn-group-vertical" role="group">
            <g:link action="index" class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span> <g:message code="default.list.label" args="[entityName]" /></g:link>
            <g:link action="create" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> <g:message code="default.add.label" args="[entityName]" /></g:link>
        </div>
    </div>
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title text-center"><g:message code="default.show.label" args="[entityName]" /></h1>
            </div>
            <table class="table property-list ${domainClass.propertyName}">
                <tbody>
                <%  excludedProps = Event.allEvents.toList() << 'id' << 'version'
                allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
                props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) && (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true) }
                Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
                props.each { p -> %>
                <g:if test="\${${propertyName}?.${p.name}}">
                    <tr>
                        <td id="${p.name}-label" class="property-label"><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></td>
                        <%  if (p.isEnum()) { %>
                        <td class="property-value" aria-labelledby="${p.name}-label"><g:fieldValue bean="\${${propertyName}}" field="${p.name}"/></td>
                        <%  } else if (p.oneToMany || p.manyToMany) { %>
                        <g:each in="\${${propertyName}.${p.name}}" var="${p.name[0]}">
                            <td class="property-value" aria-labelledby="${p.name}-label"><g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${p.name[0]}.id}">\${${p.name[0]}?.encodeAsHTML()}</g:link></td>
                        </g:each>
                        <%  } else if (p.manyToOne || p.oneToOne) { %>
                        <td class="property-value" aria-labelledby="${p.name}-label"><g:link controller="${p.referencedDomainClass?.propertyName}" action="show" id="\${${propertyName}?.${p.name}?.id}">\${${propertyName}?.${p.name}?.encodeAsHTML()}</g:link></td>
                        <%  } else if (p.type == Boolean || p.type == boolean) { %>
                        <td class="property-value" aria-labelledby="${p.name}-label"><g:formatBoolean boolean="\${${propertyName}?.${p.name}}" /></td>
                        <%  } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
                        <td class="property-value" aria-labelledby="${p.name}-label"><g:formatDate date="\${${propertyName}?.${p.name}}" /></td>
                        <%  } else if (!p.type.isArray()) { %>
                        <td class="property-value" aria-labelledby="${p.name}-label"><g:fieldValue bean="\${${propertyName}}" field="${p.name}"/></td>
                        <%  } %>
                    </tr>
                </g:if>
                <%  } %>
                </tbody>
            </table>
            <div class="panel-body">
                <fieldset class="form-actions">
                    <g:link class="btn btn-warning" action="edit" resource="\${${propertyName}}"><span class="glyphicon glyphicon-edit"></span> <g:message code="default.button.edit.label" default="Edit" /></g:link>
                    <button type="button" class="btn btn-danger" onclick="muestraModalBorrar(\${${propertyName}.id})"><span class="glyphicon glyphicon-trash"></span></button>
                </fieldset>
            </div>
        </div>
    </div>
</div>
<g:render template="/modales/modalBorrar"/>
</body>
</html>
