<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
    <title><g:message code="default.create.label" args="[entityName]" /></title>
</head>
<body>
<g:if test="\${flash.message}">
    <div class="alert alert-info alert-dismissible fade in col-md-offset-8" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong>\${flash.message}</strong>
    </div>
</g:if>

<div id="create-${domainClass.propertyName}" class="content scaffold-create" role="main">
    <div class="col-md-2">
        <div class="btn-group-vertical" role="group">
            <g:link action="index" class="btn btn-default"><span class="glyphicon glyphicon-th-list"></span> <g:message code="default.list.label" args="[entityName]" /></g:link>
            <g:link action="create" class="btn btn-default"><span class="glyphicon glyphicon-plus"></span> <g:message code="default.add.label" args="[entityName]" /></g:link>
        </div>
    </div>
    <div class="col-md-10">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h1 class="panel-title text-center"><g:message code="default.create.label" args="[entityName]" /></h1>
            </div>
            <div class="panel-body">
                <g:form url="[resource:${propertyName}, action:'save']" class="form-horizontal" <%= multiPart ? ' enctype="multipart/form-data"' : '' %>>
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="form-action">
                    <div class="pull-left">
                        <g:submitButton name="create" class="btn btn-success" value="\${message(code: 'default.button.create.label', default: 'Create')}" />
                        <button class="btn btn-default" type="reset"><g:message code="default.button.reset.label" default="Reset" /></button>
                    </div>
                </fieldset>
                </g:form>
            </div>
        </div>
    </div>
</div>
</body>
</html>
