<% import grails.persistence.Event %>
<%=packageName%>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <g:set var="entityName" value="\${message(code: '${domainClass.propertyName}.label', default: '${className}')}" />
    <title><g:message code="default.list.label" args="[entityName]" /></title>
</head>
<body>
<g:if test="\${flash.message}">
    <div class="alert alert-info alert-dismissible fade in col-md-offset-8" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
        <strong>\${flash.message}</strong>
    </div>
</g:if>
<div id="list-${domainClass.propertyName}" class="content scaffold-list" role="main">
    <div class="panel panel-default">
        <div class="panel-heading">
            <h1 class="panel-title text-center"><g:message code="default.list.label" args="[entityName]" /></h1>
        </div>
        <div class="panel-body">
            <div class="pull-right">
                <g:link action="create" class="btn btn-sm btn-success"><span class="glyphicon glyphicon-plus"></span> <g:message code="default.add.label" args="[entityName]" /></g:link>
            </div>
        </div>
        <table class="table table-condensed table-hover">
            <thead>
            <tr>
                <%  excludedProps = grails.persistence.Event.allEvents.toList() << 'id' << 'version'
                allowedNames = domainClass.persistentProperties*.name << 'dateCreated' << 'lastUpdated'
                props = domainClass.properties.findAll { allowedNames.contains(it.name) && !excludedProps.contains(it.name) && it.type != null && !Collection.isAssignableFrom(it.type) && (domainClass.constrainedProperties[it.name] ? domainClass.constrainedProperties[it.name].display : true) }
                Collections.sort(props, comparator.constructors[0].newInstance([domainClass] as Object[]))
                props.eachWithIndex { p, i ->
                    if (i < 7) {
                        if (p.isAssociation()) { %>
                <th><g:message code="${domainClass.propertyName}.${p.name}.label" default="${p.naturalName}" /></th>
                <%      } else { %>
                <g:sortableColumn property="${p.name}" title="\${message(code: '${domainClass.propertyName}.${p.name}.label', default: '${p.naturalName}')}" />
                <%  }   }   } %>
                <th><g:message code="default.action.label" default="Action" /></th>
            </tr>
            </thead>
            <tbody>
            <g:each in="\${${propertyName}List}" status="i" var="${propertyName}">
                <tr class="\${(i % 2) == 0 ? 'even' : 'odd'}">
                    <%  props.eachWithIndex { p, i ->
                        if (i < 7) {
                            if (p.type == Boolean || p.type == boolean) { %>
                    <td><g:formatBoolean boolean="\${${propertyName}.${p.name}}" /></td>
                    <%          } else if (p.type == Date || p.type == java.sql.Date || p.type == java.sql.Time || p.type == Calendar) { %>
                    <td><g:formatDate date="\${${propertyName}.${p.name}}" /></td>
                    <%          } else { %>
                    <td>\${fieldValue(bean: ${propertyName}, field: "${p.name}")}</td>
                    <%  }   }   } %>
                    <td>
                        <g:link action="show" id="\${${propertyName}.id}" class="btn btn-sm btn-primary"><g:message code="default.button.show.label"/> <span>»</span></g:link>
                        <g:link action="edit" id="\${${propertyName}.id}" class="btn btn-sm btn-warning" data-toggle="tooltip" data-placement="top" title="\${message(code: 'default.edit.label', args: [entityName])}"><span class="glyphicon glyphicon-edit"></span></g:link>
                        <button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" data-placement="top" title="\${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="muestraModalBorrar(\${${propertyName}.id})"><span class="glyphicon glyphicon-trash"></span></button>
                    </td>
                </tr>
            </g:each>
            </tbody>
        </table>
    </div>
    <div class="pagination">
        <bs:paginate total="\${${propertyName}Count ?: 0}" />
    </div>
</div>
<g:render template="/modales/modalBorrar"/>
</body>
</html>
