<html>

<head>
    <meta name='layout' content='main'/>
    <title>Buscar Usuario</title>
</head>

<body>
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center"><span class="glyphicon glyphicon-search"></span> Buscar usuario</h3>
        </div>
        <div class="panel-body">
            <g:form action='userSearch' class="form-horizontal" role="form" name='userSearchForm'>
                <div class="form-group">
                    <label for="username" class="col-md-3 control-label">Nombre de usuario</label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <input type="text" name="username" class="form-control" id="username" placeholder="Ingrese un nombre de usuario" autofocus="true">
                            <span class="input-group-btn">
                                <g:submitButton name="Buscar" class="btn btn-success" elementId='search' form='userSearchForm'/>
                            </span>
                        </div>
                    </div>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th></th>
                        <th>Verdadero</th>
                        <th>Falso</th>
                        <th>Cualquiera de los dos</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>Habilitado</td>
                        <g:radioGroup name='enabled' labels="['','','']" values="[1,-1,0]" value='${enabled}'>
                            <td class="text-center"><%=it.radio%></td>
                        </g:radioGroup>
                    </tr>
                    <tr>
                        <td>Cuenta expirada</td>
                        <g:radioGroup name='accountExpired' labels="['','','']" values="[1,-1,0]" value='${accountExpired}'>
                            <td class="text-center"><%=it.radio%></td>
                        </g:radioGroup>
                    </tr>
                    <tr>
                        <td>Cuenta bloqueada</td>
                        <g:radioGroup name='accountLocked' labels="['','','']" values="[1,-1,0]" value='${accountLocked}'>
                            <td class="text-center"><%=it.radio%></td>
                        </g:radioGroup>
                    </tr>
                    <tr>
                        <td>Contraseña expirada</td>
                        <g:radioGroup name='passwordExpired' labels="['','','']" values="[1,-1,0]" value='${passwordExpired}'>
                            <td class="text-center"><%=it.radio%></td>
                        </g:radioGroup>
                    </tr>
                    </tbody>
                </table>
            </g:form>

            <g:if test='${searched}'>

                <% def queryParams = [username: username, enabled: enabled, accountExpired: accountExpired, accountLocked: accountLocked, passwordExpired: passwordExpired] %>

                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <g:sortableColumn property="username" title="${message(code: 'user.username.label', default: 'Username')}" params="${queryParams}"/>
                        <g:sortableColumn property="enabled" title="${message(code: 'user.enabled.label', default: 'Enabled')}" params="${queryParams}"/>
                        <g:sortableColumn property="accountExpired" title="${message(code: 'user.accountExpired.label', default: 'Account Expired')}" params="${queryParams}"/>
                        <g:sortableColumn property="accountLocked" title="${message(code: 'user.accountLocked.label', default: 'Account Locked')}" params="${queryParams}"/>
                        <g:sortableColumn property="passwordExpired" title="${message(code: 'user.passwordExpired.label', default: 'Password Expired')}" params="${queryParams}"/>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${results}" status="i" var="user">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="edit" id="${user.id}">${fieldValue(bean: user, field: "username")}</g:link></td>
                            <td><g:formatBoolean boolean="${user.enabled}"/></td>
                            <td><g:formatBoolean boolean="${user.accountExpired}"/></td>
                            <td><g:formatBoolean boolean="${user.accountLocked}"/></td>
                            <td><g:formatBoolean boolean="${user.passwordExpired}"/></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="paginateButtons">
                    <bs:paginate total="${totalCount}" params="${queryParams}" />
                </div>

                <bs:paginationSummary total="${totalCount}"/>

            </g:if>

        </div>
    </div>
</div>
</body>
</html>