<html>

<head>
    <meta name='layout' content='main'/>
    <title>Editar Usuario</title>
</head>

<body>

<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Editar usuario</h3>
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist" id="miTab">
                <li class="active"><a href="#usuarios" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Usuarios</a></li>
                <li><a href="#roles" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-lock"></span> Roles</a></li>
            </ul>
            <g:form action="update" name='userEditForm' class="form-horizontal" role="form">
                <g:hiddenField name="id" value="${user?.id}"/>
                <g:hiddenField name="version" value="${user?.version}"/>
                <div class="tab-content">
                    <br>
                    <div class="tab-pane active" id="usuarios">
                        <div class="form-group">
                            <label for="username" class="col-md-3 control-label">Usuario</label>
                            <div class="col-md-8">
                                <g:textField name="username" id="username" class="form-control" value="${user?.username}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-3 control-label">Contraseña</label>
                            <div class="col-md-8">
                                <g:field type="password" name='password' class="form-control" id="password" value="${user?.password}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label">E-mail</label>
                            <div class="col-md-8">
                                <g:field type="email" name='email' class="form-control" id="email" value="${user?.email}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="enabled" class="col-md-3 control-label">Permitido</label>
                            <div class="col-md-8"><g:checkBox name="enabled" value="${user?.enabled}"/></div>
                        </div>
                        <div class="form-group">
                            <label for="accountExpired" class="col-md-3 control-label">Cuenta expirada</label>
                            <div class="col-md-8"><g:checkBox name="accountExpired" value="${user?.accountExpired}"/></div>
                        </div>
                        <div class="form-group">
                            <label for="accountLocked" class="col-md-3 control-label">Cuenta bloqueada</label>
                            <div class="col-md-8"><g:checkBox name="accountLocked" value="${user?.accountLocked}"/></div>
                        </div>
                        <div class="form-group">
                            <label for="passwordExpired" class="col-md-3 control-label">Contraseña expirada</label>
                            <div class="col-md-8"><g:checkBox name="passwordExpired" value="${user?.passwordExpired}"/></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="roles">
                        <g:each var="entry" in="${roleMap}">
                            <div>
                                <g:checkBox name="${entry.key.authority}" value="${entry.value}"/>
                                <g:link controller='role' action='edit' id='${entry.key.id}'>${entry.key.authority.encodeAsHTML()}</g:link>
                            </div>
                        </g:each>
                    </div>
                </div>
            </g:form>
        </div>
    </div>

    <g:submitButton name="update" value="${message(code: 'default.button.update.label', default: 'Update')}" class="btn btn-default" elementId='update' form='userEditForm' />
    <button type="button" id="deleteButton" class="btn btn-danger" onclick="muestraModalBorrar()"><span class="glyphicon glyphicon-trash"></span></button>

    <!-- Modal -->
    <div class="modal fade" id="modalBorrar" tabindex="-1" role="dialog" aria-labelledby="modalBorrar" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
                </div>
                <div class="modal-body">
                    <g:message code="default.button.delete.confirm.message" default="Are you sure?" />
                </div>
                <div class="modal-footer">
                    <g:form action="delete" method="POST">
                        <g:hiddenField name="id" value="${user.id}" />
                        <fieldset class="buttons">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <g:actionSubmit id="deleteButton" class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
                        </fieldset>
                    </g:form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function muestraModalBorrar() {
        $('#modalBorrar').modal('show');
    }
</script>

</body>
</html>