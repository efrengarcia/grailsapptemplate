<html>

<head>
    <meta name='layout' content='main'/>
    <title>Crear Usuario</title>
</head>

<body>
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Crear usuario</h3>
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist" id="miTab">
                <li class="active"><a href="#usuarios" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Usuarios</a></li>
                <li><a href="#roles" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-lock"></span> Roles</a></li>
            </ul>
            <g:form action="save" name='userCreateForm' class="form-horizontal" role="form">
                <div class="tab-content">
                    <br>
                    <div class="tab-pane active" id="usuarios">
                        <div class="form-group">
                            <label for="username" class="col-md-3 control-label">Usuario</label>
                            <div class="col-md-8">
                                <g:textField name="username" id="username" class="form-control" value="${user?.username}" autofocus="true"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="col-md-3 control-label">Contraseña</label>
                            <div class="col-md-8">
                                <g:field type="password" name='password' class="form-control" id="password" value="${user?.password}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="email" class="col-md-3 control-label">E-mail</label>
                            <div class="col-md-8">
                                <g:field type="email" name='email' class="form-control" id="email" value="${user?.email}"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="enabled" class="col-md-3 control-label">Permitido</label>
                            <div class="col-md-8"><g:checkBox name="enabled" value="${user?.enabled}"/></div>
                        </div>
                        <div class="form-group">
                            <label for="accountExpired" class="col-md-3 control-label">Cuenta expirada</label>
                            <div class="col-md-8"><g:checkBox name="accountExpired" value="${user?.accountExpired}"/></div>
                        </div>
                        <div class="form-group">
                            <label for="accountLocked" class="col-md-3 control-label">Cuenta bloqueada</label>
                            <div class="col-md-8"><g:checkBox name="accountLocked" value="${user?.accountLocked}"/></div>
                        </div>
                        <div class="form-group">
                            <label for="passwordExpired" class="col-md-3 control-label">Contraseña expirada</label>
                            <div class="col-md-8"><g:checkBox name="passwordExpired" value="${user?.passwordExpired}"/></div>
                        </div>
                    </div>
                    <div class="tab-pane" id="roles">
                        <g:each var="auth" in="${authorityList}">
                            <div>
                                <g:checkBox name="${auth.authority}" />
                                <g:link controller='role' action='edit' id='${auth.id}'>${auth.authority.encodeAsHTML()}</g:link>
                            </div>
                        </g:each>
                    </div>
                </div>
            </g:form>
        </div>
    </div>

    <g:submitButton name="Crear" class="btn btn-primary" elementId='create' form='userCreateForm' />

</div>
</body>
</html>