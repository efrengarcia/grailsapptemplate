<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Iniciar sesión</title>
    <asset:stylesheet src="login.css"/>
</head>

<body>
<form class="form-signin" role="form" action='${postUrl}' method='POST' id="loginForm" name="loginForm" autocomplete='off'>
    <h2 class="form-signin-heading"><g:message code='spring.security.ui.login.signin'/></h2>
    <g:textField name="j_username" class="form-control" id="username" placeholder="Usuario" required="" autofocus=""/>
    <g:field name="j_password" type="password" class="form-control" id="password" placeholder="Contraseña" required=""/>
    <div class="checkbox">
        <label>
            <input type="checkbox" name='${rememberMeParameter}' id='remember_me' <g:if test='${hasCookie}'>checked='checked'</g:if> > <g:message code='spring.security.ui.login.rememberme'/> |
        <g:link controller='register' action='forgotPassword'><g:message code='spring.security.ui.login.forgotPassword'/></g:link>

        </label>
    </div>
    <button class="btn btn-success" type="submit" elementId='loginButton' form='loginForm' messageCode='spring.security.ui.login.login'><g:message code='spring.security.ui.login.login'/></button>
    <g:link controller="register">
        <button class="btn btn-default" type="button" elementId='loginButton' messageCode='spring.security.ui.login.register'><g:message code='spring.security.ui.login.register'/></button>
    </g:link>
</form>
</body>
</html>