<div class="modal fade" id="modalBorrar" tabindex="-1" role="dialog" aria-labelledby="modalBorrar" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
            </div>
            <div class="modal-body">
                <g:message code="default.button.delete.confirm.message" default="Are you sure?" />
            </div>
            <div class="modal-footer">
                <g:form action="delete" method="DELETE">
                    <g:hiddenField name="id" />
                    <fieldset class="buttons">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                        <g:actionSubmit class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
                    </fieldset>
                </g:form>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function muestraModalBorrar(id) {
        $("input[name='id']").val(id);
        $('#modalBorrar').modal('show');
    }
</script>