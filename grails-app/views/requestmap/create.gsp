<html>
<head>
    <meta name='layout' content='main'/>
    <title>Crear Acceso</title>
</head>
<body>
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Crear Acceso</h3>
        </div>
        <div class="panel-body">
            <g:form action="save" name='requestmapCreateForm' class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="url" class="col-md-2 control-label">URL</label>
                    <div class="col-md-10">
                        <g:textField name="url" id="url" class="form-control" value="${requestmap?.url}" autofocus="true"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="configAttribute" class="col-md-2 control-label">Atributo</label>
                    <div class="col-md-10">
                        <g:textField name="configAttribute" id="configAttribute" class="form-control" value="${requestmap?.configAttribute}" />
                    </div>
                </div>
            </g:form>
        </div>
    </div>
    <g:submitButton name="Crear" class="btn btn-primary" elementId='create' form='requestmapCreateForm' />
</div>
</body>
</html>