<html>
<head>
    <meta name='layout' content='main'/>
    <title>Editar Acceos</title>
</head>
<body>
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Editar Acceso</h3>
        </div>
        <div class="panel-body">
            <g:form action='update' name='requestmapEditForm' class="form-horizontal" role="form">
                <g:hiddenField name="id" value="${requestmap?.id}"/>
                <g:hiddenField name="version" value="${requestmap?.version}"/>
                <div class="form-group">
                    <label for="url" class="col-md-2 control-label">URL</label>
                    <div class="col-md-10">
                        <g:textField name="url" id="url" class="form-control" value="${requestmap?.url}" />
                    </div>
                </div>
                <div class="form-group">
                    <label for="configAttribute" class="col-md-2 control-label">Atributo</label>
                    <div class="col-md-10">
                        <g:textField name="configAttribute" id="configAttribute" class="form-control" value="${requestmap?.configAttribute}" />
                    </div>
                </div>
            </g:form>
        </div>
    </div>
    <g:submitButton name="Actualizar" class="btn btn-default" elementId='update' form='requestmapEditForm' />
    <button type="button" id="deleteButton" class="btn btn-danger" onclick="muestraModalBorrar()"><span class="glyphicon glyphicon-trash"></span></button>
    <!-- Modal -->
    <div class="modal fade" id="modalBorrar" tabindex="-1" role="dialog" aria-labelledby="modalBorrar" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
                </div>
                <div class="modal-body">
                    <g:message code="default.button.delete.confirm.message" default="Are you sure?" />
                </div>
                <div class="modal-footer">
                    <g:form action="delete" method="POST">
                        <g:hiddenField name="id" value="${requestmap?.id}"/>
                        <fieldset class="buttons">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <g:actionSubmit id="deleteButton" class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
                        </fieldset>
                    </g:form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function muestraModalBorrar() {
        $('#modalBorrar').modal('show');
    }
</script>

</body>
</html>