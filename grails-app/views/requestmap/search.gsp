<html>

<head>
    <meta name='layout' content='main'/>
    <title>Buscar Acceso</title>
</head>

<body>

<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center"><span class="glyphicon glyphicon-search"></span> Buscar acceso</h3>
        </div>

        <div class="panel-body">
            <g:form action='requestmapSearch' name='requestmapSearchForm' class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="url" class="col-md-2 control-label">URL</label>
                    <div class="col-md-10"><g:textField name="url" id="url" class="form-control" value="${url}" /></div>
                </div>
                <div class="form-group">
                    <label for="configAttribute" class="col-md-2 control-label">Atributo</label>
                    <div class="col-md-10">
                        <g:textField name="configAttribute" id="configAttribute" class="form-control" value="${configAttribute}" />
                    </div>
                </div>
            </g:form>
            <g:submitButton name="Búsqueda" class="btn btn-success" elementId='search' form='requestmapSearchForm' />

            <br>
            <br>

            <g:if test='${searched}'>

                <%
                    def queryParams = [url: url, configAttribute: configAttribute]
                %>

                <table class="table table-bordered table-hover">
                    <thead>
                    <tr>
                        <g:sortableColumn property="url" title="${message(code: 'requestmap.url.label', default: 'URL')}" params="${queryParams}"/>
                        <g:sortableColumn property="configAttribute" title="${message(code: 'requestmap.configAttribute.label', default: 'Config Attribute')}" params="${queryParams}"/>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each in="${results}" status="i" var="requestmap">
                        <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                            <td><g:link action="edit" id="${requestmap.id}">${fieldValue(bean: requestmap, field: "url")}</g:link></td>
                            <td>${fieldValue(bean: requestmap, field: "configAttribute")}</td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>

                <div class="paginateButtons">
                    <bs:paginate total="${totalCount}" params="${queryParams}" />
                </div>

                <bs:paginationSummary total="${totalCount}"/>

            </g:if>

        </div>
    </div>
</div>
</body>
</html>