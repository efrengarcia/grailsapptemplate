<div class="collapse navbar-collapse">
    <ul class="nav navbar-nav">
        <li><g:link uri="/">Inicio</g:link></li>
        <sec:ifAnyGranted roles="ROLE_DESA">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Desarrollador <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li><g:link uri="/status">Estatus</g:link></li>
                </ul>
            </li>
        </sec:ifAnyGranted>
    </ul>
    <ul class="nav navbar-nav navbar-right">
        <sec:ifAnyGranted roles="ROLE_ADMIN">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">Seguridad <span class="caret"></span></a>
                <ul class="dropdown-menu" role="menu">
                    <li class="dropdown-submenu">
                        <g:link controller="user">Usuarios</g:link>
                        <ul class="dropdown-menu">
                            <li><g:link controller="user" action="search">Buscar</g:link></li>
                            <li><g:link controller="user" action="create">Nuevo usuario</g:link></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <g:link controller="role">Roles</g:link>
                        <ul class="dropdown-menu">
                            <li><g:link controller="role" action="search">Buscar</g:link></li>
                            <li><g:link controller="role" action="create">Nuevo rol</g:link></li>
                        </ul>
                    </li>
                    <li class="dropdown-submenu">
                        <g:link controller="requestmap">Accesos</g:link>
                        <ul class="dropdown-menu">
                            <li><g:link controller="requestmap" action="search">Buscar</g:link></li>
                            <li><g:link controller="requestmap" action="create">Nuevo acceso</g:link></li>
                        </ul>
                    </li>
                </ul>
            </li>
        </sec:ifAnyGranted>
        <g:render template="/nav/usuario" />
    </ul>
</div>