<g:set var="usuario" value="${com.infra.seguridad.SegUsuario.findByUsername(sec.loggedInUserInfo(field:'username'))}"/>
<sec:ifNotLoggedIn>
    <li><g:link controller="login"><span class="glyphicon glyphicon-user"></span> Iniciar sesión</g:link></li>
</sec:ifNotLoggedIn>
<sec:ifLoggedIn>
    <li class="dropdown">
        <a class="dropdown-toggle" role="button" data-toggle="dropdown" data-target="#" href="#">
            <span class="glyphicon glyphicon-user"></span> ${usuario?.username} <span class="caret"></span>
        </a>
        <ul class="dropdown-menu" role="menu">
            <li>
                <a href="#" id="logout">
                    <span class="glyphicon glyphicon-off"></span> Cerrar sesión
                </a>
            </li>
        </ul>
    </li>
    <g:form name="logoutForm" controller="logout"></g:form>
</sec:ifLoggedIn>