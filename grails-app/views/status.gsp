<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <title>Estatus</title>
</head>
<body>

<div id="status" class="col-md-2">
    <h4>Application Status</h4>
    <ul class="list-group">
        <li class="list-group-item">App version: <g:meta name="app.version"/></li>
        <li class="list-group-item">Grails version: <g:meta name="app.grails.version"/></li>
        <li class="list-group-item">Groovy version: ${GroovySystem.getVersion()}</li>
        <li class="list-group-item">JVM version: ${System.getProperty('java.version')}</li>
        <li class="list-group-item">Reloading active: ${grails.util.Environment.reloadingAgentEnabled}</li>
        <li class="list-group-item">Controllers: ${grailsApplication.controllerClasses.size()}</li>
        <li class="list-group-item">Domains: ${grailsApplication.domainClasses.size()}</li>
        <li class="list-group-item">Services: ${grailsApplication.serviceClasses.size()}</li>
        <li class="list-group-item">Tag Libraries: ${grailsApplication.tagLibClasses.size()}</li>
    </ul>
    <h4>Installed Plugins</h4>
    <ul class="list-group">
        <g:each var="plugin" in="${applicationContext.getBean('pluginManager').allPlugins}">
            <li class="list-group-item">${plugin.name} - ${plugin.version}</li>
        </g:each>
    </ul>
</div>
<div class="col-md-8">

    <div class="col-md-6">
        <h2>Available Controllers:</h2>
        <ul class="nav nav-pills nav-stacked">
            <g:each var="c" in="${grailsApplication.controllerClasses.sort { it.fullName } }">
                <li role="presentation"><g:link controller="${c.logicalPropertyName}">${c.fullName}</g:link></li>
            </g:each>
        </ul>
    </div>
</div>
</body>
</html>
