<!DOCTYPE html>
<html>
<head>
    <meta name='layout' content='main'/>
    <title><g:message code='spring.security.ui.forgotPassword.title'/></title>
</head>
<body>
<div class="col-md-6 col-md-offset-3">
    <g:if test="${flash.error}">
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            ${flash.error}
        </div>
    </g:if>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title text-center"><g:message code="spring.security.ui.forgotPassword.header" /></h3>
        </div>
        <div class="panel-body">
            <g:if test='${emailSent}'>
                <h5 class="text-center"><g:message code='spring.security.ui.forgotPassword.sent'/></h5>
            </g:if>
            <g:else>
                <h5 class="text-center"><g:message code='spring.security.ui.forgotPassword.description'/></h5>
                <g:form name="forgotPasswordForm" action="forgotPassword" autocomplete="off">
                    <div class="form-group">
                        <label class="control-label" for="username"><g:message code="spring.security.ui.forgotPassword.username" default="Username"/></label>
                        <input type="text" name="username" class="form-control" id="username" placeholder="Ingrese su nombre de usuario" autofocus>
                    </div>
                    <g:submitButton name="reset" value="${message(code: 'spring.security.ui.forgotPassword.submit', default: 'Reset')}" class="btn btn-lg btn-success" />
                </g:form>
            </g:else>
        </div>
    </div>
</div>

</body>
</html>
