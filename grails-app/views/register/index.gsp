<!DOCTYPE html>
<html>
<head>
    <meta name='layout' content='main'/>
    <title><g:message code='spring.security.ui.register.title'/></title>
</head>
<body>
<div class="col-md-6 col-md-offset-3">
    <g:hasErrors bean="${command}">
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <g:renderErrors bean="${command}" as="list" />
        </div>
    </g:hasErrors>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title text-center"><g:message code="spring.security.ui.register.description" /></h3>
        </div>
        <div class="panel-body">
            <g:if test='${emailSent}'>
                <h5 class="text-center"><g:message code='spring.security.ui.register.sent'/></h5>
            </g:if>
            <g:else>
                <g:form name="registerForm" action="register">
                    <div class="form-group ${hasErrors(bean: command, field: 'username', 'has-error')}">
                        <label class="control-label" for="username"><g:message code="user.username.label" default="Username"/></label>
                        <input type="text" name="username" class="form-control" id="username" placeholder="Ingrese nombre de usuario" value="${command.username}" autofocus>
                    </div>
                    <div class="form-group ${hasErrors(bean: command, field: 'email', 'has-error')}">
                        <label class="control-label" for="email"><g:message code="user.email.label" default="Email" /></label>
                        <input type="email" name="email" class="form-control" id="email" placeholder="Ingrese correo electrónico" value="${command.email}">
                    </div>
                    <div class="form-group ${hasErrors(bean: command, field: 'password', 'has-error')}">
                        <label class="control-label" for="password"><g:message code="user.password.label" default="Password" /></label>
                        <input type="password" name="password" class="form-control" id="password" placeholder="Ingrese contraseña" value="${command.password}" >
                    </div>
                    <div class="form-group ${hasErrors(bean: command, field: 'password2', 'has-error')}">
                        <label class="control-label" for="password2"><g:message code="user.passwordConfirm.label" default="Password (again)" /></label>
                        <input type="password" name="password2" class="form-control" id="password2" placeholder="Confirme contraseña" value="${command.password2}" >
                    </div>

                    <g:submitButton name="create" value="${message(code: 'spring.security.ui.register.submit', default: 'Register')}" class="btn btn-lg btn-success" />
                </g:form>
            </g:else>
        </div>
    </div>
</div>
</body>
</html>