<!DOCTYPE html>
<html>
<head>
    <meta name='layout' content='main'/>
    <title><g:message code='spring.security.ui.resetPassword.title'/></title>
</head>
<body>
<div class="col-md-6 col-md-offset-3">
    <g:hasErrors bean="${command}">
        <div class="alert alert-danger alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span></button>
            <g:renderErrors bean="${command}" as="list" />
        </div>
    </g:hasErrors>
    <div class="panel panel-default">
        <div class="panel-heading">
            <h3 class="panel-title text-center"><g:message code="spring.security.ui.resetPassword.header" /></h3>
        </div>
        <div class="panel-body">
            <h5 class="text-center"><g:message code='spring.security.ui.resetPassword.description'/></h5>
            <g:form name="resetPasswordForm" action="resetPassword" autocomplete="off">
                <g:hiddenField name='t' value='${token}'/>
                <div class="form-group ${hasErrors(bean: command, field: 'password', 'has-error')}">
                    <label class="control-label" for="password"><g:message code="resetPasswordCommand.password.label" default="Password"/></label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Ingrese su nueva contraseña" value="${command.password}" autofocus>
                </div>
                <div class="form-group ${hasErrors(bean: command, field: 'password2', 'has-error')}">
                    <label class="control-label" for="password2"><g:message code="resetPasswordCommand.password2.label" default="Password (again)"/></label>
                    <input type="password" name="password2" class="form-control" id="password2" placeholder="Repita su nueva contraseña" value="${command.password2}" >
                </div>
                <g:submitButton name="reset" value="${message(code: 'spring.security.ui.resetPassword.submit', default: 'Update')}" class="btn btn-lg btn-success" />
            </g:form>
        </div>
    </div>
</div>
</body>
</html>