<html>

<head>
    <meta name='layout' content='main'/>
    <title>Crear Rol</title>
</head>

<body>

<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Crear rol</h3>
        </div>
        <div class="panel-body">
            <g:form action="save" name='roleCreateForm' class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="authority" class="col-sm-2 control-label">Autoridad</label>
                    <div class="col-md-10">
                        <g:textField name="authority" id="authority" class="form-control" value="${role?.authority}" />
                    </div>
                </div>
            </g:form>
        </div>
    </div>
    <g:submitButton name="Crear" class="btn btn-primary" elementId='create' form='roleCreateForm' />
</div>

</body>
</html>