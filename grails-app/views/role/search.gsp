<html>

<head>
    <meta name='layout' content='main'/>
    <title>Buscar Rol</title>
</head>

<body>

<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center"><span class="glyphicon glyphicon-search"></span> Buscar rol</h3>
        </div>
        <div class="panel-body">
            <g:form action='roleSearch' name='roleSearchForm' class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="authority" class="col-md-3 control-label">Autoridad</label>
                    <div class="col-md-8">
                        <div class="input-group">
                            <input type="text" name="authority" class="form-control" id="authority" placeholder="Ingrese un rol" value="${role?.authority}" autofocus="true">
                            <span class="input-group-btn">
                                <g:submitButton name="Buscar" class="btn btn-success" elementId='search' form='roleSearchForm'/>
                            </span>
                        </div>
                    </div>
                </div>
            </g:form>

        <!-- Resultados -->

            <g:if test='${searched}'>

                <%
                    def queryParams = [authority: authority]
                %>

                <div class="list">
                    <table class="table table-bordered table-hover">
                        <thead>
                        <tr>
                            <g:sortableColumn property="authority" title="${message(code: 'role.authority.label', default: 'Authority')}" params="${queryParams}"/>
                        </tr>
                        </thead>

                        <tbody>
                        <g:each in="${results}" status="i" var="role">
                            <tr class="${(i % 2) == 0 ? 'odd' : 'even'}">
                                <td><g:link action="edit" id="${role.id}">${fieldValue(bean: role, field: "authority")}</g:link></td>
                            </tr>
                        </g:each>
                        </tbody>
                    </table>
                </div>

                <div class="paginateButtons">
                    <bs:paginate total="${totalCount}" params="${queryParams}" />
                </div>

                <bs:paginationSummary total="${totalCount}"/>

            </g:if>
        </div>
    </div>
</div>

</body>
</html>