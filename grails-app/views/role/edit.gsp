<html>
<head>
    <meta name='layout' content='main'/>
    <title>Editar Rol</title>
</head>
<body>
<div class="col-md-8 col-md-offset-2">
    <div class="panel panel-primary">
        <div class="panel-heading">
            <h3 class="panel-title text-center">Editar rol</h3>
        </div>
        <div class="panel-body">
            <ul class="nav nav-tabs" role="tablist" id="myTab">
                <li class="active"><a href="#roles" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-lock"></span> Roles</a></li>
                <li><a href="#usuarios" role="tab" data-toggle="tab"><span class="glyphicon glyphicon-user"></span> Usuarios</a></li>
            </ul>
            <div class="tab-content">
                <br>
                <div class="tab-pane active" id="roles">
                    <g:form action="update" name='roleEditForm' class="form-horizontal" role="form">
                        <g:hiddenField name="id" value="${role?.id}"/>
                        <g:hiddenField name="version" value="${role?.version}"/>
                        <div class="form-group">
                            <label for="authority" class="col-sm-2 control-label">Autoridad</label>
                            <div class="col-md-10">
                                <g:textField name="authority" id="authority" class="form-control" value="${role?.authority}"/>
                            </div>
                        </div>
                    </g:form>

                </div>
                <div class="tab-pane" id="usuarios">
                    <g:if test='${users.empty}'>
                        <h4 class="text-center text-danger">No existen usuarios asignados a este rol</h4>
                    </g:if>
                    <ul class="list-group col-md-4 col-md-offset-4">
                        <g:each var="u" in="${users}">
                            <li class="list-group-item"><g:link controller='user' action='edit' id='${u.id}'>${u.username.encodeAsHTML()}</g:link></li>
                        </g:each>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <g:submitButton name="Actualizar" class="btn btn-default" elementId='update' form='roleEditForm' />
    <button type="button" id="deleteButton" class="btn btn-danger" onclick="muestraModalBorrar()"><span class="glyphicon glyphicon-trash"></span></button>
    <!-- Modal -->
    <div class="modal fade" id="modalBorrar" tabindex="-1" role="dialog" aria-labelledby="modalBorrar" aria-hidden="true">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title" id="myModalLabel">Eliminar Registro</h4>
                </div>
                <div class="modal-body">
                    <g:message code="default.button.delete.confirm.message" default="Are you sure?" />
                </div>
                <div class="modal-footer">
                    <g:form action="delete" method="POST">
                        <g:hiddenField name="id" value="${role.id}" />
                        <fieldset class="buttons">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
                            <g:actionSubmit id="deleteButton" class="btn btn-danger" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" />
                        </fieldset>
                    </g:form>
                </div>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function muestraModalBorrar() {
        $('#modalBorrar').modal('show');
    }
</script>

</body>
</html>