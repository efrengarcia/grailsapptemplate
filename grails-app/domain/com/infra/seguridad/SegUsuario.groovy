package com.infra.seguridad

class SegUsuario {

	transient springSecurityService

	String username
	String password
    String email
	boolean enabled = true
	boolean accountExpired
	boolean accountLocked
	boolean passwordExpired

	static transients = ['springSecurityService']

	static constraints = {
		username blank: false, unique: true
		password blank: false
        email nullable: false, blank: false, unique: true, email: true
	}

	static mapping = {
		password column: '`password`'
	}

	Set<SegRol> getAuthorities() {
		SegUsuarioSegRol.findAllBySegUsuario(this).collect { it.segRol }
	}

	def beforeInsert() {
		encodePassword()
	}

	def beforeUpdate() {
		if (isDirty('password')) {
			encodePassword()
		}
	}

	protected void encodePassword() {
		password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword(password) : password
	}
}
