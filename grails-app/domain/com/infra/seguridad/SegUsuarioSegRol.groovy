package com.infra.seguridad

import org.apache.commons.lang.builder.HashCodeBuilder

class SegUsuarioSegRol implements Serializable {

	private static final long serialVersionUID = 1

	SegUsuario segUsuario
	SegRol segRol

	boolean equals(other) {
		if (!(other instanceof SegUsuarioSegRol)) {
			return false
		}

		other.segUsuario?.id == segUsuario?.id &&
		other.segRol?.id == segRol?.id
	}

	int hashCode() {
		def builder = new HashCodeBuilder()
		if (segUsuario) builder.append(segUsuario.id)
		if (segRol) builder.append(segRol.id)
		builder.toHashCode()
	}

	static SegUsuarioSegRol get(long segUsuarioId, long segRolId) {
		SegUsuarioSegRol.where {
			segUsuario == SegUsuario.load(segUsuarioId) &&
			segRol == SegRol.load(segRolId)
		}.get()
	}

	static boolean exists(long segUsuarioId, long segRolId) {
		SegUsuarioSegRol.where {
			segUsuario == SegUsuario.load(segUsuarioId) &&
			segRol == SegRol.load(segRolId)
		}.count() > 0
	}

	static SegUsuarioSegRol create(SegUsuario segUsuario, SegRol segRol, boolean flush = false) {
		def instance = new SegUsuarioSegRol(segUsuario: segUsuario, segRol: segRol)
		instance.save(flush: flush, insert: true)
		instance
	}

	static boolean remove(SegUsuario u, SegRol r, boolean flush = false) {
		if (u == null || r == null) return false

		int rowCount = SegUsuarioSegRol.where {
			segUsuario == SegUsuario.load(u.id) &&
			segRol == SegRol.load(r.id)
		}.deleteAll()

		if (flush) { SegUsuarioSegRol.withSession { it.flush() } }

		rowCount > 0
	}

	static void removeAll(SegUsuario u, boolean flush = false) {
		if (u == null) return

		SegUsuarioSegRol.where {
			segUsuario == SegUsuario.load(u.id)
		}.deleteAll()

		if (flush) { SegUsuarioSegRol.withSession { it.flush() } }
	}

	static void removeAll(SegRol r, boolean flush = false) {
		if (r == null) return

		SegUsuarioSegRol.where {
			segRol == SegRol.load(r.id)
		}.deleteAll()

		if (flush) { SegUsuarioSegRol.withSession { it.flush() } }
	}

	static constraints = {
		segRol validator: { SegRol r, SegUsuarioSegRol ur ->
			if (ur.segUsuario == null) return
			boolean existing = false
			SegUsuarioSegRol.withNewSession {
				existing = SegUsuarioSegRol.exists(ur.segUsuario.id, r.id)
			}
			if (existing) {
				return 'userRole.exists'
			}
		}
	}

	static mapping = {
		id composite: ['segRol', 'segUsuario']
		version false
	}
}
