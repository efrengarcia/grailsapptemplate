import com.infra.seguridad.SegAcceso
import com.infra.seguridad.SegRol
import com.infra.seguridad.SegUsuario
import com.infra.seguridad.SegUsuarioSegRol

class BootStrap {

    def init = { servletContext ->
        // define roles y usuarios
        SegRol adminRol = new SegRol(authority: 'ROLE_ADMIN').save(flush: true)
        SegRol desaRol = new SegRol(authority: 'ROLE_DESA').save(flush: true)
        SegUsuario adminUsuario = new SegUsuario(username: 'admin', password: '1234', email: 'efren.garcia.guarneros@gmail.com').save(flush: true)
        SegUsuario desaUsuario = new SegUsuario(username: 'desa', password: '1234', email: 'efren.garcia@outlook.com').save(flush: true)
        SegUsuarioSegRol.create adminUsuario, adminRol, true
        SegUsuarioSegRol.create desaUsuario, desaRol, true
        // validación de usuarios y roles
        assert SegUsuario.count() == 2
        assert SegRol.count() == 2
        assert SegUsuarioSegRol.count() == 2
        // define los accesos de la aplicación
        for (String url in [
                '/', '/index', '/index.gsp', '/**/favicon.ico',
                '/assets/**', '/**/js/**', '/**/css/**', '/**/images/**',
                '/login', '/login.*', '/login/*', '/register/**',
                '/logout', '/logout.*', '/logout/*']) {
            new SegAcceso(url: url, configAttribute: 'permitAll').save()
        }
        for (String url in [
                '/status'
        ]) {
            new SegAcceso(url: url, configAttribute: 'ROLE_DESA').save()
        }
        // permisos para la seguridad
        for (String url in [
                '/aclclass/**',
                '/aclentry/**',
                '/aclobjectidentity/**',
                '/aclsid/**',
                '/persistentlogin/**',
                '/registrationcode/**',
                '/requestmap/**',
                '/role/**',
                '/securityinfo/**',
                '/user/**',
                '/persona/**'
        ]) {
            new SegAcceso(url: url, configAttribute: 'ROLE_ADMIN').save()
        }
    }
    def destroy = {
    }
}
